import regex
from os import getcwd, listdir, makedirs
from os.path import exists
import PIL.Image as pillow
from shutil import move

class File:

	def __init__(self, name, path=getcwd(), newpath=None):
		self.name = name
		self.type = regex.search(r"\.[\w]+", str(self.name)).group(0)[1::]
		self.path = path
		self.newpath = newpath

		with pillow.open(self.name) as img:
			date = img._getexif()
		if date[306]:
			self.created_at = f"{date[306][0:4]}/{date[306][5:7]}/{date[306][8:10]}/{date[306][11:13]}"
		else:
			self.created_at = None

		self.newpath=f"{self.path}/{self.created_at}"

	def put_in_subfolder(self):
		if exists(self.newpath):
			move(self.name, self.newpath)
		else:
			makedirs(self.newpath)
			move(self.name, self.newpath)